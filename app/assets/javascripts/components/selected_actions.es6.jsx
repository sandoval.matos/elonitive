const SelectedActions = React.createClass({
  initiativeTable () {
    const creatures = []

    if (this.props.actions) {
      for (const c of this.props.actions) {
        creatures.push(<CreatureRow
          creature={ c.creature }
          actions={ c.actions }
          initiative={ c.initiative }
          key={ c.creature.id }
          dm={ this.props.dm }
        />)
      }
    }

    return creatures
  },
  render () {
    return (
      <div>
        { this.initiativeTable() }
        {/* { JSON.stringify(this.props.actions) } */}
      </div>
    )
  }
})

const CreatureRow = React.createClass({
  selectedActions () {
    actions = []

    if (this.props.actions) {
      for (const a of this.props.actions) {
        if (!(!this.props.creature.playable && !this.props.dm)) {
          actions.push(<SelectedAction action={a} key={a.available_action_id}/>)
        }
      }
    }

    return actions
  },
  alertFeatRow() {
    if (this.props.creature.alert_feat) {
      return (
        <div className="row eachAction">
          <div className="col-3 col-md-2">
            { formatRoll(-5) }
          </div>
          <div className="col-9 col-md-10">
            Alerta (talento)
          </div>
        </div>
      )
    }

    return ""
  },
  dexterityModRow() {
    if (!(!this.props.creature.playable && !this.props.dm)) {
      return (
        <div className="row eachAction">
          <div className="col-3 col-md-2">
            { formatRoll(this.signal((this.props.creature.dex_mod) * -1)) }
          </div>
          <div className="col-9 col-md-10">
            Mod. Destreza
          </div>
        </div>
      )
    }

    return ""
  },
  signal (i) {
    return ((i >= 0) ? `+${i}` : `${i}`)
  },
  isCurrentTurn() {
    if (this.props.creature.active_turn) {
      return (
        <span className="float-right">
          <i className="fa fa-circle"></i>
        </span>
      )
    } else {
      return ""
    }
  },
  render () {
    return (
      <div className="selectedActions">
        <div className="row">
          <div className="col-3 col-md-2">
            <h5>
              { formatRoll(this.props.initiative) }
            </h5>
          </div>
          <div className="col-9 col-md-10">
            <h5>
              { this.props.creature.name }
              { this.isCurrentTurn() }
            </h5>
          </div>
        </div>

        { this.selectedActions() }
        { this.dexterityModRow() }
        { this.alertFeatRow() }
      </div>
    )
  }
})

const SelectedAction = React.createClass({
  render () {
    return (
      <div className="row eachAction">
        <div className="col-3 col-md-2">
          { formatRoll(this.props.action.roll) }
        </div>
        <div className="col-9 col-md-10">
          { this.props.action.name }
          <strong>
            ({ this.props.action.dice })
          </strong>
        </div>
      </div>
    )
  }
})

formatRoll = r => {
  if (Array.isArray(r)) {
    return (
      <div className="row rolls">
        <div className="col-6 text-center">
          { r[0] }
        </div>
        <div className="col-6 text-center">
          { r[1] }
        </div>
      </div>
    )
  } else {
    return (
      <div className="row rolls">
        <div className="col-12 text-center">
          { r }
        </div>
      </div>
    )
  }
}
