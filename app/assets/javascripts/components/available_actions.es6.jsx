const AvailableActions = React.createClass({
  // Returns an element containing the corresponding type actions
  renderActions (type = "attack", apt = 1) {
    const actions = this.loadActionsArray(type)
    let actionsButtons = ""
    let label = ""

    if (actions.length) {
      switch (type) {
        case "attack":
          label = `Ataques (${apt} / turno)`
          break;

        case "spell":
          label = "Magias"
          break;

        default:
          label = "Demais ações"
      }

      actionsButtons = (
        <div>
          <strong>
            { label }
          </strong>
          <div className="ActionList">
            { actions }
          </div>
        </div>
      )
    }

    return actionsButtons
  },

  // Returns an array with the corresponding type actions
  loadActionsArray (action_type = "attack") {
    const actions = []

    for (const [i, a] of this.props.availableActions.entries()) {
      if (a.action_type == action_type) {
        actions.push(<ActionButton
          key={ i }
          action={ a }
          onClick={ () => this.props.handleActionSelection(a.available_action_id) }
        />)
      }
    }

    return actions
  },

  // Render everything
  render () {
    return (
      <div className="col-12 AvailableActions">
        <h5>
          Ações de Combate
        </h5>
        <div className="boxActions">
          { this.renderActions("attack", this.props.currentCreature.attacks_per_turn) }
          { this.renderActions("spell") }
          { this.renderActions("other") }
        </div>
      </div>
    );
  }
})

const ActionButton = React.createClass({
  onClick (e) {
    e.preventDefault()
    $("a").blur()
    this.props.onClick()
  },
  render () {
    return (
      <a
        href="#"
        onClick={ (e) => this.onClick(e) }
        role="button"
        className={ "btn btn-block " + ((this.props.action.selected) ? "btn-primary" : "btn-secondary") }
      >
        { this.props.action.name }
        <strong>
          { `(${this.props.action.dice})` }
        </strong>
      </a>
    )
  }
})
