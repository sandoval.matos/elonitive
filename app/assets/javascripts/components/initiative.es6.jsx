const Initiative = React.createClass({
  // Initial state
  getInitialState () {
    return {
      creatures: this.formatCreatures(this.props.creatures),
      availableActions: [],
      selectedActions: []
    }
  },

  // Prepare ActionCable
  prepareActionCable () {
    const self = this
    return (App.cable.subscriptions.create("InitiativeChannel", {
      received: function(actions) {
        self.updateState({ selectedActions: actions, availableActions: true })
        return true;
      }
    }))
  },

  // Make AJAX requests
  componentDidMount () {
    App.initiative = this.prepareActionCable()
    $.get(`/api/v1/initiative/refresh`, (actions) => {
      this.updateState({ creatures: this.state.creatures, selectedActions: actions, availableActions: true })
    })
  },

  // Formats creatures Map to future using
  formatCreatures (creatures, currentCreatureIndex = 0) {
    const newCreatures = []

    for (const [i, c] of creatures.entries()) {
      c.currentCreature = (currentCreatureIndex == i) ? true : false
      newCreatures.push(c)
    }

    return newCreatures
  },

  // Returns state's current creature
  getCurrentStateCreature () {
    return this.getCurrentCreatureFrom(this.state.creatures)
  },

  // Return the current creature from an array
  getCurrentCreatureFrom (creatures) {
    for (const c of creatures) {
      if (c.currentCreature) {
        return c
      }
    }
  },

  // Sets current creature
  setCurrentCreature (creatureIndex) {
    const previousCreature = this.getCurrentCreatureFrom(this.state.creatures)
    const newCreatures = this.formatCreatures(this.state.creatures, creatureIndex)
    const newCreature = this.getCurrentCreatureFrom(newCreatures)

    if(previousCreature != newCreature) {
      this.updateState({creatures: newCreatures, availableActions: true })
    }
  },

  updateState (params) {
    const newState = {
      creatures: this.state.creatures,
      availableActions: this.state.availableActions,
      selectedActions: this.state.selectedActions
    }

    if (params.creatures) {
      newState.creatures = params.creatures
    }

    if (params.selectedActions) {
      newState.selectedActions = params.selectedActions
    }

    if(params.availableActions) {
      const currentCreature = this.getCurrentCreatureFrom(newState.creatures)
      $.get(`/api/v1/creatures/${currentCreature.id}/actions`, (newActions) => {
        newState.availableActions = newActions

        this.setState(newState)
      })
    } else {
      this.setState(newState)
    }
  },

  // Toggles action state (selected or not)
  toggleActionSelection(actionId) {
    $.get(`/api/v1/actions/${actionId}/toggle`)
  },

  // Render everything
  render () {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 col-sm-6 initiativeTable">
            <SelectedActions
              actions={ this.state.selectedActions }
              dm={ this.props.dm }
            />
            <InitiativeButtons
              dm={ this.props.dm } />
          </div>
          <div className="col-12 col-sm-6 sideBar">
            <div className="row">
              <SelectCreature
                creatures={ this.state.creatures }
                currentCreature={ this.getCurrentStateCreature() }
                onSelectCreature={ creatureId => this.setCurrentCreature(creatureId) } />
              <AvailableActions
                currentCreature={ this.getCurrentStateCreature() }
                availableActions={ this.state.availableActions }
                handleActionSelection={ actionId => this.toggleActionSelection(actionId) } />
            </div>
          </div>
        </div>
      </div>
    );
  }
});
