const InitiativeButtons = React.createClass({
  onClick (e, action) {
    e.preventDefault()
    $("a").blur()

    if (action == "roll") {
      $.get(`/api/v1/initiative/roll`)
    } else if (action == "reset") {
      $.get(`/api/v1/initiative/reset`)
    } else if (action == "next_turn") {
      $.get(`/api/v1/initiative/next_turn`)
    }
  },
  render () {
    if (this.props.dm) {
      return (
        <div className="row InitiativeButtons">
          <div className="col-12 button-holder">
            <a
              href="#"
              onClick={ (e) => this.onClick(e, "roll") }
              role="button"
              className="btn btn-primary float-right">
              Rolar
            </a>
            <a
              href="#"
              onClick={ (e) => this.onClick(e, "reset") }
              role="button"
              className="btn btn-secondary float-right">
              Resetar
            </a>
            <a
              href="#"
              onClick={ (e) => this.onClick(e, "next_turn") }
              role="button"
              className="btn btn-primary float-left">
              Próximo
            </a>
          </div>
        </div>
      )
    } else {
      return (
        <div className="row InitiativeButtons">
          <div className="col-12 button-holder">
          </div>
        </div>
      )
    }
  }
})
