class Creature < ApplicationRecord

  has_many :available_actions
  has_many :selected_actions

  def self.calculate_dex_mod dex
    return ((dex - 10) / 2).floor
  end

  def get_used_initiative
    if advantage && (initiative[0].to_i > initiative[1].to_i)
      initiative[1].to_i
    else
      initiative[0].to_i
    end
  end

end
