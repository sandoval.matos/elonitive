class CombatAction < ApplicationRecord

  has_many :available_actions
  has_many :selected_actions

  enum action_type: [ :attack, :spell, :other ]

end
