class InitiativeController < ApplicationController
  def index
    @creatures = Creature.where(playable: true).order(:name)
  end

  def dm
    if params[:creatures_type] == "pcs"
      @creatures = Creature.where(playable: true)
    elsif params[:creatures_type] == "npcs"
      @creatures = Creature.where(playable: false)
    else
      @creatures = Creature.all
    end
    @creatures.order(:name, :playable)
  end
end
