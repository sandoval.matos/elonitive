class Api::V1::InitiativeController < ApplicationController
  def refresh
    # Empty table
    initiative_table = []

    # Get all the creatures with at least one action selected
    Creature
      .joins(:available_actions)
      .where(available_actions: { selected: true })
      .distinct
      .each do |creature|
        # Initiative starts in 0
        creature_initiative = [0, 0]

        # Get the selected actions for the current creature
        selected_actions = AvailableAction.where(selected: true, creature: creature)

        # Sum each action used
        selected_actions.each do |selected_action|
          creature_initiative[0] += selected_action[:roll][0].to_i
          creature_initiative[1] += selected_action[:roll][1].to_i
        end

        # Subtract DEX mod
        creature_initiative[0] -= creature.dex_mod
        creature_initiative[1] -= creature.dex_mod

        # If creature has Alert feat, subtract 5 from initiative
        if creature.alert_feat
          creature_initiative[0] -= 5
          creature_initiative[1] -= 5
        end

        # If it's less than 1, then it's 1
        creature_initiative[0] = 1 if creature_initiative[0] < 1
        creature_initiative[1] = 1 if creature_initiative[1] < 1

        # Update Creature record
        creature[:initiative] = creature_initiative
        creature.save

        # Join information from both action tables
        actions = []
        selected_actions.each do |sa|
          ca = CombatAction.find(sa[:combat_action_id])
          actions << {
            available_action_id: sa[:id],
            combat_action_id: ca[:id],
            name: ca[:name],
            dice: ca[:dice],
            roll: sa[:roll]
          }
        end

        # Get actual initiative
        initiative_number = creature.get_used_initiative

        # Insert into initiative table
        initiative_table << {
          initiative: initiative_number,
          creature: creature,
          actions: actions
        }
      end

    # Sort by initiative (from lowest to highest)
    # then, by dexterity (from highest to lowest)
    initiative_table = initiative_table.sort_by { |t|
      [ t[:initiative], (t[:creature][:dexterity] * -1) ]
    }

    # Broadcast new initiative table to websocket
    ActionCable.server.broadcast 'initiative', initiative_table

    render json: initiative_table
  end

  def next_turn
    # Get creatures where there is at least one selected actions
    creatures = []
    Creature.where.not(initiative: []).each do |c|
      creatures << {
        creature: c,
        initiative: c.get_used_initiative
      }
    end

    # Order creature list
    creatures = creatures.sort_by { |c|
      [ c[:initiative], (c[:creature][:dexterity] * -1) ]
    }

    if creatures.reject { |c| !c[:creature][:active_turn] }.empty?
      creatures.first[:creature].update(active_turn: true)
    else
      index = 0

      # Loop through creatures to get current creature
      creatures.each_with_index do |c, i|
        if c[:creature][:active_turn]
          unless creatures[i+1].nil?
            index = i+1
          end
          creatures[i][:creature].update(active_turn: false)
        end
      end

      creatures[index][:creature].update(active_turn: true)
    end

    refresh
  end

  # Reset everything
  def reset
    # Deselect actions, dices rolled and initiative count
    AvailableAction.update_all(selected: false, roll: [])
    Creature.update_all(initiative: [])

    refresh
  end

  def clear
    AvailableAction.update_all(roll: [])
    Creature.update_all(initiative: [])

    refresh
  end

  # Roll for initiative!
  def roll
    # Roll action dices
    roll_action_dices

    # Call refresh method
    refresh
  end

  # Roll multiple dices
  # Param: [XdY, ..] ([1d4, 2d20], [8d6])
  def roll_dices dices
    results = []
    dices.each do |d|
      results << roll_dice(d)
    end
    results
  end

  # Roll one dice
  # Param: XdY (1d4, 2d20)
  def roll_dice dice
    t = dice.split "d"
    dice_number = t[0].to_i

    d = t[1].split "+"
    dice_faces = d[0].to_i
    dice_mod = d[1].to_i

    return (dice_number.times.collect { rand(1..dice_faces) }.reduce(:+) + dice_mod)
  end

  private
    # Roll actions dices
    def roll_action_dices
      actions = AvailableAction.where(selected: true)

      actions.each do |a|
        ca = CombatAction.find_by(id: a.combat_action_id)
        a.update(roll: roll_dices([ca.dice, ca.dice]))
      end
    end
end
