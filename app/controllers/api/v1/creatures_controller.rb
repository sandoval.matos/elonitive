class Api::V1::CreaturesController < Api::V1::InitiativeController
  def actions
    render json: CombatAction
      .joins(:available_actions)
      .select("combat_actions.id, combat_actions.name, combat_actions.dice, combat_actions.action_type, available_actions.id as available_action_id, available_actions.selected")
      .where(available_actions: { creature_id: params[:id] })
      .order("combat_actions.name")
  end

end
