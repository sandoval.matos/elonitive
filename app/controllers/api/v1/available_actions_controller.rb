class Api::V1::AvailableActionsController < Api::V1::InitiativeController
  def toggle
    AvailableAction
      .find_by_id(params[:id])
      .toggle(:selected)
      .save!

    clear
  end
end
