Rails.application.routes.draw do
  root "initiative#index"
  get '/sandomestre/:creatures_type', to: "initiative#dm"

  namespace :api do
    namespace :v1 do
      get '/initiative/roll', to: 'initiative#roll'
      get '/initiative/reset', to: 'initiative#reset'
      get '/initiative/refresh', to: 'initiative#refresh'
      get '/initiative/next_turn', to: 'initiative#next_turn'
      get '/initiative/dice/:dice', to: 'initiative#roll_dice'

      get '/creatures/:id/actions', to: 'creatures#actions'

      get '/actions/:id/toggle', to: 'available_actions#toggle'
    end
  end

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'
end
