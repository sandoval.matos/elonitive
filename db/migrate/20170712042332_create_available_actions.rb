class CreateAvailableActions < ActiveRecord::Migration[5.1]
  def change
    create_table :available_actions do |t|
      t.belongs_to :creature, index: true
      t.belongs_to :combat_action, index: true
      t.boolean :selected, default: false
      t.text :roll, array: true, default: []
    end
  end
end
