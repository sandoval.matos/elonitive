class CreateCombatActions < ActiveRecord::Migration[5.1]
  def change
    create_table :combat_actions do |t|
      t.string :name
      t.string :dice
      t.integer :action_type

      t.timestamps
    end
  end
end
