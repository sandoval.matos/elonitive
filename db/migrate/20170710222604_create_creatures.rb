class CreateCreatures < ActiveRecord::Migration[5.1]
  def change
    create_table :creatures do |t|
      t.string :name
      t.integer :dexterity
      t.integer :dex_mod
      t.boolean :alert_feat, default: false
      t.boolean :advantage, default: false
      t.integer :attacks_per_turn, default: 1
      t.boolean :playable, default: false
      t.boolean :active_turn, default: false
      t.text :initiative, array: true, default: []

      t.timestamps
    end
  end
end
