# Simple Melee Actions
CombatAction.create([
  { name: "Adaga", dice: "2d4", action_type: "attack" },
  { name: "Azagaia", dice: "2d6", action_type: "attack" },
  { name: "Bordão", dice: "2d6", action_type: "attack" },
  { name: "Clava Grande", dice: "2d8", action_type: "attack" },
  { name: "Foice Curta", dice: "2d4", action_type: "attack" },
  { name: "Lança", dice: "2d6", action_type: "attack" },
  { name: "Maça", dice: "2d6", action_type: "attack" },
  { name: "Machadinha", dice: "2d6", action_type: "attack" },
  { name: "Martelo Leve", dice: "2d4", action_type: "attack" },
  { name: "Porrete", dice: "2d4", action_type: "attack" }
])

# Simple Ranged Actions
CombatAction.create([
  { name: "Arco Curto", dice: "2d6", action_type: "attack" },
  { name: "Besta Leve", dice: "2d8", action_type: "attack" },
  { name: "Dardo", dice: "2d4", action_type: "attack" },
  { name: "Funda", dice: "2d4", action_type: "attack" }
])

# Martial Melee Weapons
CombatAction.create([
  { name: "Alabarda", dice: "2d10", action_type: "attack" },
  { name: "Chicote", dice: "2d4", action_type: "attack" },
  { name: "Cimitarra", dice: "2d6", action_type: "attack" },
  { name: "Espada Curta", dice: "2d6", action_type: "attack" },
  { name: "Espada Grande", dice: "3d6", action_type: "attack" },
  { name: "Espada Longa", dice: "2d8", action_type: "attack" },
  { name: "Glaive", dice: "2d10", action_type: "attack" },
  { name: "Lança de Montaria", dice: "2d12", action_type: "attack" },
  { name: "Lança Longa", dice: "2d10", action_type: "attack" },
  { name: "Maça Estrela", dice: "2d8", action_type: "attack" },
  { name: "Machado de Batalha", dice: "2d8", action_type: "attack" },
  { name: "Machado Grande", dice: "2d12", action_type: "attack" },
  { name: "Malho", dice: "3d6", action_type: "attack" },
  { name: "Mangual", dice: "2d8", action_type: "attack" },
  { name: "Martelo de Guerra", dice: "2d8", action_type: "attack" },
  { name: "Picareta de Guerra", dice: "2d8", action_type: "attack" },
  { name: "Rapieira", dice: "2d8", action_type: "attack" },
  { name: "Tridente", dice: "2d6", action_type: "attack" }
])

# Martial Ranged Weapons
CombatAction.create([
  { name: "Arco Longo", dice: "2d8", action_type: "attack" },
  { name: "Besta de Mão", dice: "2d6", action_type: "attack" },
  { name: "Besta Pesada", dice: "2d10", action_type: "attack" },
  { name: "Rede", dice: "--", action_type: "attack" },
  { name: "Zarabatana", dice: "1", action_type: "attack" }
])

# Class actions
CombatAction.create([
  { name: "Artes Marciais", dice: "2d4", action_type: "attack" }
])

# Monster actions
CombatAction.create([
  { name: "Garras", dice: "2d4", action_type: "attack" },
  { name: "Mordida", dice: "2d6", action_type: "attack" }
])

# Spells
CombatAction.create([
  { name: "Truque", dice: "2d6", action_type: "spell" },
  { name: "Magia (nvl. 1)", dice: "2d8+1", action_type: "spell" },
  { name: "Magia (nvl. 2)", dice: "2d8+2", action_type: "spell" },
  { name: "Magia (nvl. 3)", dice: "2d8+3", action_type: "spell" },
  { name: "Magia (nvl. 4)", dice: "2d8+4", action_type: "spell" },
  { name: "Magia (nvl. 5)", dice: "2d8+5", action_type: "spell" },
  { name: "Magia (nvl. 6)", dice: "2d8+6", action_type: "spell" },
  { name: "Magia (nvl. 7)", dice: "2d8+7", action_type: "spell" },
  { name: "Magia (nvl. 8)", dice: "2d8+8", action_type: "spell" },
  { name: "Magia (nvl. 9)", dice: "2d8+9", action_type: "spell" }
])

# Other actions
CombatAction.create({ name: "Movimentação", dice: "2d4", action_type: "other" })
CombatAction.create({ name: "Demais Ações", dice: "2d6", action_type: "other" })

# PCs
bohrn = Creature.create(
  name: "Bohrn",
  dexterity: 8,
  dex_mod: Creature.calculate_dex_mod(8),
  playable: true
)

hidekunn = Creature.create(
  name: "Hidekunn",
  dexterity: 17,
  dex_mod: Creature.calculate_dex_mod(17),
  attacks_per_turn: 2,
  playable: true
)

khaoks = Creature.create(
  name: "Khaoks",
  dexterity: 15,
  dex_mod: Creature.calculate_dex_mod(15),
  playable: true
)

miau = Creature.create(
  name: "Miau",
  dexterity: 16,
  dex_mod: Creature.calculate_dex_mod(16),
  playable: true
)

nohrmao = Creature.create(
  name: "Nohr'Mao",
  dexterity: 16,
  alert_feat: true,
  advantage: true,
  dex_mod: Creature.calculate_dex_mod(16),
  playable: true
)

ozurin = Creature.create(
  name: "Ozurin Kheon",
  dexterity: 14,
  dex_mod: Creature.calculate_dex_mod(14),
  attacks_per_turn: 2,
  playable: true
)

panda = Creature.create(
  name: "Panda Grylls",
  dexterity: 16,
  dex_mod: Creature.calculate_dex_mod(16),
  playable: true
)

tolin = Creature.create(
  name: "Tolin Ovak",
  dexterity: 10,
  dex_mod: Creature.calculate_dex_mod(10),
  playable: true
)

# Adding available_actions to PCs
AvailableAction.create([
  { creature: bohrn, combat_action: CombatAction.find_by_name("Espada Longa") },
  { creature: bohrn, combat_action: CombatAction.find_by_name("Martelo de Guerra") },
  { creature: bohrn, combat_action: CombatAction.find_by_name("Truque") },
  { creature: bohrn, combat_action: CombatAction.find_by_name("Magia (nvl. 1)") },
  { creature: bohrn, combat_action: CombatAction.find_by_name("Magia (nvl. 2)") },
  { creature: bohrn, combat_action: CombatAction.find_by_name("Magia (nvl. 3)") }
])

AvailableAction.create([
  { creature: hidekunn, combat_action: CombatAction.find_by_name("Arco Curto") },
  { creature: hidekunn, combat_action: CombatAction.find_by_name("Artes Marciais") },
  { creature: hidekunn, combat_action: CombatAction.find_by_name("Bordão") },
  { creature: hidekunn, combat_action: CombatAction.find_by_name("Dardo") }
])

AvailableAction.create([
  { creature: khaoks, combat_action: CombatAction.find_by_name("Besta Leve") },
  { creature: khaoks, combat_action: CombatAction.find_by_name("Truque") },
  { creature: khaoks, combat_action: CombatAction.find_by_name("Magia (nvl. 1)") },
  { creature: khaoks, combat_action: CombatAction.find_by_name("Magia (nvl. 2)") }
])

AvailableAction.create([
  { creature: miau, combat_action: CombatAction.find_by_name("Garras") },
  { creature: miau, combat_action: CombatAction.find_by_name("Mordida") }
])

AvailableAction.create([
  { creature: nohrmao, combat_action: CombatAction.find_by_name("Arco Longo") },
  { creature: nohrmao, combat_action: CombatAction.find_by_name("Azagaia") },
  { creature: nohrmao, combat_action: CombatAction.find_by_name("Machadinha") },
  { creature: nohrmao, combat_action: CombatAction.find_by_name("Magia (nvl. 1)") }
])

AvailableAction.create([
  { creature: ozurin, combat_action: CombatAction.find_by_name("Cimitarra") },
  { creature: ozurin, combat_action: CombatAction.find_by_name("Maça Estrela") },
  { creature: ozurin, combat_action: CombatAction.find_by_name("Machadinha") },
  { creature: ozurin, combat_action: CombatAction.find_by_name("Rapieira") },
  { creature: ozurin, combat_action: CombatAction.find_by_name("Truque") },
  { creature: ozurin, combat_action: CombatAction.find_by_name("Magia (nvl. 1)") }
])

AvailableAction.create([
  { creature: panda, combat_action: CombatAction.find_by_name("Arco Curto") },
  { creature: panda, combat_action: CombatAction.find_by_name("Espada Longa") },
  { creature: panda, combat_action: CombatAction.find_by_name("Truque") },
  { creature: panda, combat_action: CombatAction.find_by_name("Magia (nvl. 1)") }
])

AvailableAction.create([
  { creature: tolin, combat_action: CombatAction.find_by_name("Espada Longa") },
  { creature: tolin, combat_action: CombatAction.find_by_name("Azagaia") },
  { creature: tolin, combat_action: CombatAction.find_by_name("Magia (nvl. 1)") }
])

# Some NPCs
guard = Creature.create(
  name: "Guarda",
  dexterity: 14,
  dex_mod: Creature.calculate_dex_mod(14),
  playable: false
)

AvailableAction.create([
  { creature: guard, combat_action: CombatAction.find_by_name("Espada Longa") },
  { creature: guard, combat_action: CombatAction.find_by_name("Besta Leve") }
])

Creature.all.each do |c|
  AvailableAction.create([
    { creature: c, combat_action: CombatAction.find_by_name("Demais Ações") },
    { creature: c, combat_action: CombatAction.find_by_name("Movimentação") }
  ])
end
